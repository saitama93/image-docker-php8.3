# Image PHP-FPM avec Composer

Ce dépôt contient le Dockerfile et les fichiers nécessaires pour construire une image Docker PHP-FPM avec Composer installé. Cette image est basée sur `bitnami/php-fpm:8.3`.

## Contenu du Dockerfile

```Dockerfile
# Utiliser l'image PHP-FPM comme base
FROM bitnami/php-fpm:8.3

# Installer les dépendances nécessaires et Composer
RUN apt-get update && \
    apt-get install -y unzip curl && \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Copier les fichiers de votre application dans le conteneur
COPY . /app

# Définir le répertoire de travail
WORKDIR /app
```

## Instructions pour construire l'image

### Cloner le dépôt :

```sh
git clone https://gitlab.com/saitama93/image-docker-php8.3.git
```

### Construire l'image Docker :

```sh
docker build -t zorodono/php8.3:latest .
```

### Tester l'image localement :

```sh
docker run -p 9000:9000 zorodono/php8.3:latest
```

### Utilisation de l'image

Pour utiliser cette image dans vos projets, ajoutez simplement la ligne suivante à votre fichier docker-compose.yml ou utilisez la commande docker run :


```yml
services:
  php:
    image: zorodono/php8.3:latest
    ports:
      - "9000:9000"
    volumes:
      - .:/app
```