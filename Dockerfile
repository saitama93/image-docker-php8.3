# Utiliser l'image PHP-FPM comme base
FROM bitnami/php-fpm:8.3

# Installer les dépendances nécessaires et Composer
RUN apt-get update && \
    apt-get install -y unzip curl && \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Copier les fichiers de votre application dans le conteneur
COPY . /app

# Définir le répertoire de travail
WORKDIR /app
